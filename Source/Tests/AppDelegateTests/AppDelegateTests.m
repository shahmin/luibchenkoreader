//
//  AppDelegateTests.m
//  NIXProject
//
//  Created by Egor Zubkov on 1/22/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#import "AppDelegateTests.h"
#import "AppDelegate.h"
#import "Flurry.h"

@implementation AppDelegateTests

#pragma mark -
#pragma mark GHTestCase methods

- (BOOL)shouldRunOnMainThread
{
    return YES;
}

#pragma mark -
#pragma mark Tests

- (void)testWindowProperty
{
    AppDelegate *appDelegate = [AppDelegate new];
    
    UIWindow *window = [UIWindow new];
    
    // exercise
    [appDelegate setWindow:window];
    
    // verify
    GHAssertEqualObjects(window, [appDelegate window], @"");
}

@end
